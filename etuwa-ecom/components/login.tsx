import React, {SyntheticEvent, useState} from 'react';
import {useRouter} from "next/router";
import { userLogin } from '../pages/api/auth';
import { useCookies } from "react-cookie"
import styles from '../styles/registration.module.css'
import { cartDetails } from '../pages/api/cartinfo';

const Login = () => {
    const [cookie, setCookie, removeCookie] = useCookies(["user"]);
    const [cartcookie, setCartCookie, removeCartCookie] = useCookies(["cartcount"]);
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const router = useRouter();

    const submit =  async (e: SyntheticEvent) => {
        e.preventDefault();
        removeCookie("user",{
            path: "/"
        });
        removeCartCookie("cartcount",{
            path: "/"
        });
        var userData: any = await userLogin(email,password);
        console.log(userData.data.name);
        if(!!userData.data.login) {
            let cartData: any = await cartDetails(userData.data.access_token);

            setCookie("user", JSON.stringify(userData.data), {
                path: "/",
                maxAge: 3600, // Expires after 1hr
                sameSite: true,
            });

            setCartCookie("cartcount", JSON.stringify(cartData.cartResponse.product_count), {
                path: "/",
                maxAge: 3600, // Expires after 1hr
                sameSite: true,
            });
            
            await router.push('/');
            router.push({
                pathname: '/185/en',
            })
        }    
    }

    return (
            <div className={styles.loginWrapper}>
            <h4>Login</h4>
                <form onSubmit={submit}>
                    <div>
                        <input type="email" className={styles.inputField} placeholder="Email" required
                        onChange={e => setEmail(e.target.value)} />
                    </div>
                    <div>
                        <input type="password"  className={styles.inputField} placeholder="Password" required
                            onChange={e => setPassword(e.target.value)}
                        />
                    </div>

                    <button className={styles.greenButton} type="submit">Sign in</button>
                </form>
            </div>
    );
};

export default Login;