import styles from '../styles/main.module.css'
import Image from 'next/image'
import Link from 'next/link'
import { homeURL } from '../utilities/urls'
import { parseCartCookie, parseCookies } from "../helpers/parseCookie"

const Header = () => {
    return (
        <div className={styles.navWrapper}>
            <div className={styles.inlineWrapper}>
                <Link href={homeURL()}><a className={styles.headerLabels}>Home</a></Link>
                <Link href={homeURL()}><a className={styles.headerLabels}>Home</a></Link>
                <Link href={homeURL()}><a className={styles.headerLabels}>Home</a></Link>
                <p className={styles.navCaption}> All you need is just one click away... </p>
            </div>
            <div className={`${styles.checkoutWrapper} ${styles.inlineWrapper} `}>
                <span>{parseCookies().name}</span>
                <button type="button" className={styles.checkoutButton}>Checkout <span>{parseCartCookie()}</span></button>
            </div>
        </div>
    );
}

export default Header;