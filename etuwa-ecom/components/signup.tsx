import React, {SyntheticEvent, useState} from 'react';
import {useRouter} from "next/router";
import { userLogin } from '../pages/api/auth';
import styles from '../styles/registration.module.css'

const Signup = () => {
    let countryData = [];
    countryData = [
        { value: 'USA', name: 'USA' },
        { value: 'CANADA', name: 'CANADA' }            
    ];
    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [contact, setContact] = useState('');
    const [address, setAddress] = useState('');
    const [area, setArea] = useState('');

    const router = useRouter();

    const submit =  async (e: SyntheticEvent) => {
        e.preventDefault();
        let userData: any = await userLogin(email,password);
        console.log(userData.data.name);
        if(!!userData.data.login) {
            //await router.push('/');
            router.push({
                pathname: '/185/en',
            })
        }    
    }

    return (
            <div className={styles.loginWrapper}>
            <h4>Signup</h4>
                <form onSubmit={submit}>
                    <div>
                        <input type="text"  className={styles.inputField} placeholder="Name" required
                            onChange={e => setName(e.target.value)}
                        />
                    </div>
                    <div>
                        <input type="email" className={styles.inputField} placeholder="Email" required
                        onChange={e => setEmail(e.target.value)} />
                    </div>
                    <div>
                        <input type="password"  className={styles.inputField} placeholder="Password" required
                            onChange={e => setPassword(e.target.value)}
                        />
                    </div>
                    <div>
                        <input type="tel" className={styles.inputField} placeholder="Contact No." required
                        onChange={e => setContact(e.target.value)} />
                    </div>
                    <div>
                        <textarea  name="address" rows={4} cols={50} className={styles.inputField} placeholder="Address" required
                        onChange={e => setAddress(e.target.value)} />
                    </div>
                    <div>
                        <select name="country" onChange={e => setArea(e.target.value)} value={""}>
                            {countryData.map((e, key) => {
                                return <option key={key} value={e.value}>{e.name}</option>;
                            })}
                        </select>
                    </div>


                    <button className={styles.greenButton} type="submit">Register</button>
                </form>
            </div>
    );
};

export default Signup;