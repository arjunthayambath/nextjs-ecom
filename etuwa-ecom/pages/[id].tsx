/**
 Used to identify hwmny HTML pages needs to be created
 Author: Sumith M S
 Component name: ItemDetails
 **/
 import React, { useState } from "react"
 import styles from '../styles/item.module.css'
 import Image from 'next/image'
 import {baseURL} from '../utilities/urls'
 import {replaceBackSlash} from '../utilities/utilities'

export async function getServerSideProps(context: any) {
    const id = context.params.id;
    const res = await fetch('https://qfreshonline.com/androidapp/json/ProductSingle?id=' + id);
    const data = await res.json();

    if (!data) {
    return {
        notFound: true,
    }
    }

    return {
        props: { item: data } // will be passed to the page component as props
    }
}


//ItemDetails component starts from here
const ItemDetails = ({ item }: { item: any }) => {
    const [isURLCopied, setState] = useState(false)
    return (
        <div>
            <div className={styles.inlineWrapper}>
                <div className={styles.inlineWrapper}>
                    <Image loader={myLoader} src={replaceBackSlash(item.main_image)} width={400} height={400} alt="item image"/>
                </div>
                <div className={`${styles.inlineWrapper} ${styles.imageList}`}>
                    <div className={styles.imageWrapper}><Image loader={myLoader} src={replaceBackSlash(item.main_image)} width={400} height={400} alt="item image"/></div>
                    <div className={styles.imageWrapper}><Image loader={myLoader} src={replaceBackSlash(item.main_image)} width={400} height={400} alt="item image"/></div>
                    <div className={styles.imageWrapper}><Image loader={myLoader} src={replaceBackSlash(item.main_image)} width={400} height={400} alt="item image"/></div>
                </div>
            </div>
            <div className={`${styles.inlineWrapper} ${styles.itemDetails}`}>
                <h1>{ item.name }</h1>
                <h1>{ item.arabic_name }</h1>
                {!!item.min_order_quantity ? <h4><span>Min order : </span>{ item.min_order_quantity }</h4> : null }
                <h4 className={(item.discount_price != item.price) ? styles.discountPrice : ""}><span>Price: </span>{ replaceBackSlash(item.price_formatted) }</h4>
                {(item.discount_price != item.price) ? <h4><span>Discount Price: </span>{ replaceBackSlash(item.discount_price_formatted) }</h4> : null}
                {/* <button type="button" className={styles.greenButton}>Add to cart</button> */}
                <button className={`${styles.greenButton} ${styles.shareButton}`} onClick={handleClick}>{(isURLCopied) ? "Thank You" : "Share" }</button>
                <span>{(isURLCopied) ? " - URL copied to clipboard" : ""}</span>
            </div>
            
        </div>
    );

    function handleClick(this: any): void {
        let urlTextArea = document.createElement("textarea");
        document.body.appendChild(urlTextArea);
        urlTextArea.value = baseURL()+item.id;
        urlTextArea.select();
        document.execCommand("copy");
        document.body.removeChild(urlTextArea);
        setState(!isURLCopied);
    }

    function myLoader() {
        return `${replaceBackSlash(item.main_image)}`;
    }

}

export default ItemDetails;