export async function userLogin (email: string, password: string) {
    const res = await fetch('https://qfreshonline.com/androidapp/json/login', {
            method: 'POST',
            //credentials: 'include',
            body: JSON.stringify({
                email,
                password
            })
        })
        
        const data = await res.json();
        return { data } // will be passed to the page component as props
}

export async function userRegistration (email: string, password: string) {
    const res = await fetch('https://qfreshonline.com/androidapp/json/login', {
            method: 'POST',
            //credentials: 'include',
            body: JSON.stringify({
                email,
                password
            })
        })
        
        const data = await res.json();
        return { data } // will be passed to the page component as props
}