export async function cartDetails (accessToken: string) {
    const cartRequest = await fetch('https://qfreshonline.com/androidapp/json/cartinfo?access_token=' + accessToken);
    const cartResponse = await cartRequest.json();
    return {cartResponse};
}    